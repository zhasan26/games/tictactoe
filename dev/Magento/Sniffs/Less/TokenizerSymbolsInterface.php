<?php
/***********************************************************************************************************************
 * @package		Learning
 * @author		Zahirul Hasan<info@zbabu.com>
 * @copyright	Copyright (c) 2011 - 2019 @ Zahirul Hasan (http://zbabu.com) All rights reserved.
 * @license		http://zbabu.com/license-agreement
 **********************************************************************************************************************/
namespace Magento\Sniffs\Less;

/**
 * Interface TokenizerSymbolsInterface
 *
 */
interface TokenizerSymbolsInterface
{
    const TOKENIZER_CSS = 'CSS';

    /**#@+
     * Symbols for usage into Sniffers
     */
    const BITWISE_AND         = '&';
    const COLON               = ';';
    const OPEN_PARENTHESIS    = '(';
    const CLOSE_PARENTHESIS   = ')';
    const NEW_LINE            = "\n";
    const WHITESPACE          = ' ';
    const DOUBLE_WHITESPACE   = '  ';
    const INDENT_SPACES       = '    ';
    /**#@-*/
}
