/***********************************************************************************************************************
 * @package		Learning
 * @author		Zahirul Hasan<info@zbabu.com>
 * @copyright	Copyright (c) 2011 - 2019 @ Zahirul Hasan (http://zbabu.com) All rights reserved.
 * @license		http://zbabu.com/license-agreement
 **********************************************************************************************************************/

Vue.component('loader', {
    props: ['isLoading'],
    template: `<div class="loader" v-bind:style="{display: [isLoading ? 'block' : 'none']}">
                  <div class="loader-container">
                      <div class="spin one"></div>
                      <div class="spin two"></div>
                      <div class="spin three"></div>
                  </div>
              </div>`
});
Vue.component('component-board', {
    props: ['boards'],
    template: `<div class="row"><component-board-item
              v-for="board in boards"
              v-bind:key="boards.id"
              v-bind:board="board"
              @player-move="(fieldId) => $emit('player-move',fieldId)"
              ></component-board-item></div>`,
});
Vue.component('component-board-item', {
    props: ['board'],
    template: `<div class="col-4 field"
              v-bind:id="board.id"
              v-on:click="board.allowMove ? $emit('player-move', board.id) : null"
              ><i v-bind:class="board.iconClass"></i></div>`
});
new Vue({
    el: '#tic-tac-toe',
    data: {
        boardData :[],
        isLoading: false
    },
    created: function () {this.reset()},
    methods: {
        reset:function() {
            this.renderBoard('/reset')
        },
        renderBoard: function (urlPart = '', prostData = {}) {
            var that = this;
            that.isLoading = true;
            $.post(
                '/tictactoe/game'+urlPart,
                prostData,
                function( data ) {
                    that.boardData = data.board;
                }
            ).always(function(){
                that.isLoading = false;
            });
        },
        playerMove: function (fieldId) {
            this.renderBoard('/move',{fieldId:fieldId});
        }
    }
});