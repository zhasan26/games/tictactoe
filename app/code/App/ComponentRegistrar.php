<?php
/***********************************************************************************************************************
 * @package		Learning
 * @author		Zahirul Hasan<info@zbabu.com>
 * @copyright	Copyright (c) 2011 - 2019 @ Zahirul Hasan (http://zbabu.com) All rights reserved.
 * @license		http://zbabu.com/license-agreement
 **********************************************************************************************************************/

namespace TicTacToe\App;

/**
 * Class ComponentRegistrar
 * @package TicTacToe\App
 */
class ComponentRegistrar
{
    /**
     * @var array
     */
    private static $paths = [];

    /**
     * @var array
     */
    private static $routes = [];

    /**
     * @param      $componentName
     * @param      $path
     * @param null $route
     */
    public static function register($componentName, $path, $route = null)
    {
        if (isset(self::$paths[$componentName])) {
            throw new \LogicException(
                'Module \'' . $componentName . '\' from \'' . $path . '\' '
                . 'has been already defined in \'' . self::$paths[$componentName] . '\'.'
            );
        } elseif (!empty($route) && isset(self::$routes[$route])) {
            throw new \LogicException(
                'Route \'' . $route . '\' from \'' . $path . '\' '
                . 'has been already defined in \'' . self::$routes[$route] . '\'.'
            );
        } else {
            self::$paths[$componentName] = str_replace('\\', '/', $path);
            if (!empty($route)) {
                self::$routes[$route] = $componentName;
            }
        }
    }

    /**
     * @return array
     */
    public function getPaths()
    {
        return self::$paths;
    }

    /**
     * @return array
     */
    public function getRoutes()
    {
        return self::$routes;
    }

    /**
     * @param string $componentName
     * @return mixed|null
     */
    public function getPathByModule(string $componentName)
    {
        return isset(self::$paths[$componentName]) ? self::$paths[$componentName] : null;
    }

    /**
     * @param string $route
     * @return mixed|null
     */
    public function getModuleByRoute(string $route)
    {
        return isset(self::$routes[$route]) ? self::$routes[$route] : null;
    }

    /**
     * @param string $route
     * @return mixed|null
     */
    public function getPathByRoute(string $route)
    {
        $module = $this->getModuleByRoute($route);
        return empty($module) ? null : $this->getPathByModule($module);
    }

    /**
     * @param string $route
     * @return mixed|null
     */
    public function getNameSpaceByRoute(string $route)
    {
        $nameSpace = $this->getModuleByRoute($route);
        if (!empty($nameSpace)) {
            $nameSpace = "\\" . join("\\", \explode("_", $nameSpace));
        }
        return $nameSpace;
    }
}
