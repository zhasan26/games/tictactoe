<?php
/***********************************************************************************************************************
 * @package		Learning
 * @author		Zahirul Hasan<info@zbabu.com>
 * @copyright	Copyright (c) 2011 - 2019 @ Zahirul Hasan (http://zbabu.com) All rights reserved.
 * @license		http://zbabu.com/license-agreement
 **********************************************************************************************************************/

namespace TicTacToe\App;

/**
 * Class AbstractBlock
 * @package TicTacToe
 */
abstract class AbstractController implements ControllerInterface
{
    /**
     * @var \TicTacToe\App\BlockInterface
     */
    protected $block;

    /**
     * @var \Zend\Http\PhpEnvironment\Request
     */
    protected $request;

    /**
     * @var \Zend\Http\PhpEnvironment\Response
     */
    protected $response;

    /**
     * AbstractController constructor.
     * @param \TicTacToe\App\Context\ControllerContext $context
     * @param \TicTacToe\App\BlockInterface            $block
     */
    public function __construct(
        \TicTacToe\App\Context\ControllerContext $context,
        \TicTacToe\App\BlockInterface $block
    ) {
        $this->request = $context->getRequest();
        $this->response = $context->getResponse();
        $this->block = $block;
    }

    public function dispatch()
    {
        $html = $this->execute();
        $this->response->setContent($html);
        foreach ($this->block->getHeaders() as $headerKey => $headerValue) {
            $this->response->getHeaders()->addHeaderLine(
                $headerKey . ": " . $headerValue
            );
        }
        $this->response->send();
    }

    public function execute(): string
    {
        return $this->block->getHtml();
    }
}
