<?php
/***********************************************************************************************************************
 * @package		Learning
 * @author		Zahirul Hasan<info@zbabu.com>
 * @copyright	Copyright (c) 2011 - 2019 @ Zahirul Hasan (http://zbabu.com) All rights reserved.
 * @license		http://zbabu.com/license-agreement
 **********************************************************************************************************************/

namespace TicTacToe\App;

/**
 * Class AbstractBlock
 * @package TicTacToe
 */
abstract class AbstractBlock implements BlockInterface
{
    /**
     * @var string
     */
    protected $template;

    /**
     * @var array
     */
    protected $childBlocks;

    /**
     * @var array
     */
    protected $headers;

    /**
     * @var \TicTacToe\App\ComponentRegistrar
     */
    private $componentRegistrar;

    /**
     * AbstractBlock constructor.
     * @param \TicTacToe\App\Context\BlockContext $context
     * @param array                              $childBlocks
     */
    public function __construct(
        \TicTacToe\App\Context\BlockContext $context,
        $childBlocks = []
    ) {
        $this->childBlocks = $childBlocks;
        $this->componentRegistrar = $context->getComponentRegistrar();
        $this->headers = [];
        $this->_construct();
    }


    abstract public function _construct();

    /**
     * @return string
     */
    public function getHtml(): string
    {
        if (empty($this->getTemplate())) {
            return "";
        }
        return $this->renderTemplate();
    }

    /**
     * @return string
     */
    public function getTemplate(): string
    {
        return $this->template;
    }

    /**
     * @param string $template
     * @return \TicTacToe\BlockInterface
     */
    public function setTemplate(string $template): AbstractBlock
    {
        $this->template = $template;
        return $this;
    }

    /**
     * @return string
     */
    protected function getTemplateFile(): string
    {
        list($module, $templateFile) = \explode("::", $this->getTemplate());
        $templateFile = $this->componentRegistrar->getPathByModule($module)
                        .\DIRECTORY_SEPARATOR . 'Templates'. \DIRECTORY_SEPARATOR . $templateFile;

        return $templateFile;
    }

    /**
     * @return string
     */
    public function renderTemplate(): string
    {
        \ob_start();
        require_once $this->getTemplateFile();
        $content = \ob_get_clean();
        \ob_flush();
        return $content;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param string $key
     * @param string $value
     * @return $this
     */
    protected function addHeader(string $key, string $value)
    {
        $this->headers[$key] = $value;
        return $this;
    }
}
