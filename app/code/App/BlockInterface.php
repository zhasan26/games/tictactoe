<?php
/***********************************************************************************************************************
 * @package		Learning
 * @author		Zahirul Hasan<info@zbabu.com>
 * @copyright	Copyright (c) 2011 - 2019 @ Zahirul Hasan (http://zbabu.com) All rights reserved.
 * @license		http://zbabu.com/license-agreement
 **********************************************************************************************************************/

namespace TicTacToe\App;

interface BlockInterface
{
    /**
     * @return string
     */
    public function getHtml():string;

    /**
     * @return string
     */
    public function getTemplate():string;

    public function getHeaders(): array;
}
