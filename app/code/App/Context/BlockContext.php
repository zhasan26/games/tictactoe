<?php
/***********************************************************************************************************************
 * @package		Learning
 * @author		Zahirul Hasan<info@zbabu.com>
 * @copyright	Copyright (c) 2011 - 2019 @ Zahirul Hasan (http://zbabu.com) All rights reserved.
 * @license		http://zbabu.com/license-agreement
 **********************************************************************************************************************/

namespace TicTacToe\App\Context;

/**
 * Class Context
 * @package TicTacToe\App
 * @method \TicTacToe\App\ComponentRegistrar getComponentRegistrar()
 */
class BlockContext extends Context
{
    /**
     * BlockContext constructor.
     * @param \Zend\Http\PhpEnvironment\Request $request
     * @param \TicTacToe\App\SessionManager      $sessionManager
     * @param \TicTacToe\App\ComponentRegistrar  $componentRegistrar
     */
    public function __construct(
        \Zend\Http\PhpEnvironment\Request $request,
        \TicTacToe\App\SessionManager $sessionManager,
        \TicTacToe\App\ComponentRegistrar  $componentRegistrar
    ) {
        parent::__construct($request, $sessionManager);
        $this->setComponentRegistrar($componentRegistrar);
    }
}
