<?php
/***********************************************************************************************************************
 * @package		Learning
 * @author		Zahirul Hasan<info@zbabu.com>
 * @copyright	Copyright (c) 2011 - 2019 @ Zahirul Hasan (http://zbabu.com) All rights reserved.
 * @license		http://zbabu.com/license-agreement
 **********************************************************************************************************************/

namespace TicTacToe\App\Context;

/**
 * Class Context
 * @package TicTacToe\App
 * @method \Zend\Http\PhpEnvironment\Request getRequest()
 * @method \TicTacToe\App\SessionManager getSession()
 */
abstract class Context extends \TicTacToe\App\DataObject
{
    /**
     * Context constructor.
     * @param \Zend\Http\PhpEnvironment\Request $request
     * @param \TicTacToe\App\SessionManager      $sessionManager
     */
    public function __construct(
        \Zend\Http\PhpEnvironment\Request $request,
        \TicTacToe\App\SessionManager $sessionManager
    ) {
        parent::__construct([]);
        $this->setRequest($request);
        $this->setSession($sessionManager);
    }
}
