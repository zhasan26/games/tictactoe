<?php
/***********************************************************************************************************************
 * @package		Learning
 * @author		Zahirul Hasan<info@zbabu.com>
 * @copyright	Copyright (c) 2011 - 2019 @ Zahirul Hasan (http://zbabu.com) All rights reserved.
 * @license		http://zbabu.com/license-agreement
 **********************************************************************************************************************/

namespace TicTacToe\App\Context;

/**
 * Class Context
 * @package TicTacToe\App
 * @method \Zend\Http\PhpEnvironment\Response getResponse()
 */
class ControllerContext extends Context
{
    /**
     * ControllerContext constructor.
     * @param \Zend\Http\PhpEnvironment\Request  $request
     * @param \TicTacToe\App\SessionManager       $sessionManager
     * @param \Zend\Http\PhpEnvironment\Response $response
     */
    public function __construct(
        \Zend\Http\PhpEnvironment\Request $request,
        \TicTacToe\App\SessionManager $sessionManager,
        \Zend\Http\PhpEnvironment\Response $response
    ) {
        parent::__construct($request, $sessionManager);
        $this->setResponse($response);
    }
}
