<?php
/***********************************************************************************************************************
 * @package		Learning
 * @author		Zahirul Hasan<info@zbabu.com>
 * @copyright	Copyright (c) 2011 - 2019 @ Zahirul Hasan (http://zbabu.com) All rights reserved.
 * @license		http://zbabu.com/license-agreement
 **********************************************************************************************************************/
namespace TicTacToe\App;

class SessionManager extends \Zend\Session\SessionManager
{
    public function __construct(
        \Zend\Session\Config\SessionConfig $config,
        \Zend\Session\Storage\SessionArrayStorage $storage
    ) {
        $config->setOptions(['cookie_lifetime' => 60*60*1,'gc_maxlifetime' => 60*60*24*30]);
        parent::__construct(
            $config,
            $storage,
            null,
            [
                \Zend\Session\Validator\RemoteAddr::class,
                \Zend\Session\Validator\HttpUserAgent::class,
            ]
        );
        $this->start();
    }

    /**
     * Get Offset
     *
     * @param  mixed $key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->getStorage()->offsetGet($key);
    }

    /**
     * Set Offset
     *
     * @param  mixed $key
     * @param  mixed $value
     * @return void
     */
    public function __set($key, $value)
    {
        return $this->getStorage()->offsetSet($key, $value);
    }

    /**
     * Isset Offset
     *
     * @param  mixed   $key
     * @return bool
     */
    public function __isset($key)
    {
        return $this->getStorage()->offsetExists($key);
    }

    /**
     * Unset Offset
     *
     * @param  mixed $key
     * @return void
     */
    public function __unset($key)
    {
        return $this->getStorage()->offsetUnset($key);
    }
}
