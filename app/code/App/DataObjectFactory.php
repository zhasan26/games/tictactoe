<?php
/***********************************************************************************************************************
 * @package		Learning
 * @author		Zahirul Hasan<info@zbabu.com>
 * @copyright	Copyright (c) 2011 - 2019 @ Zahirul Hasan (http://zbabu.com) All rights reserved.
 * @license		http://zbabu.com/license-agreement
 **********************************************************************************************************************/

namespace TicTacToe\App;

/**
 * Universal data container with array access implementation
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
class DataObjectFactory
{
    /**
     * @var \TicTacToe\App\ObjectManager
     */
    protected $objectManager;

    /**
     * DataObjectFactory constructor.
     * @param \TicTacToe\App\ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param array $params
     * @return \TicTacToe\App\DataObject|null
     */
    public function get(array $params = [])
    {
        return $this->objectManager->get(\TicTacToe\App\DataObject::class, $params);
    }

    /**
     * @param array $params
     * @return \TicTacToe\App\DataObject|null
     */
    public function create(array $params = [])
    {
        return $this->objectManager->create(\TicTacToe\App\DataObject::class, $params);
    }
}
