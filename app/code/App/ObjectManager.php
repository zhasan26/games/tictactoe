<?php
/***********************************************************************************************************************
 * @package		Learning
 * @author		Zahirul Hasan<info@zbabu.com>
 * @copyright	Copyright (c) 2011 - 2019 @ Zahirul Hasan (http://zbabu.com) All rights reserved.
 * @license		http://zbabu.com/license-agreement
 **********************************************************************************************************************/

namespace TicTacToe\App;

class ObjectManager
{
    /**
     * @var \Zend\Di\Di
     */
    private $di;

    public function __construct()
    {
        $pathList = [
            BP . \DIRECTORY_SEPARATOR . 'app' . \DIRECTORY_SEPARATOR . 'etc' .
            \DIRECTORY_SEPARATOR . 'di.xml',
            BP . \DIRECTORY_SEPARATOR . 'app' . \DIRECTORY_SEPARATOR . 'code' .
            \DIRECTORY_SEPARATOR . '*' . \DIRECTORY_SEPARATOR . '*' .
            \DIRECTORY_SEPARATOR . 'etc' . \DIRECTORY_SEPARATOR . 'di.xml',
        ];
        $config = [];

        foreach ($pathList as $path) {
            $files = \glob($path, GLOB_NOSORT);
            foreach ($files as $file) {
                $config = \array_merge_recursive($config, $this->loadConfig($file));
            }
        }

        $this->di = new \Zend\Di\Di(null, null, new \Zend\Di\Config($config));
        $this->di->instanceManager()->addSharedInstance($this, self::class);
    }

    protected function loadConfig(string $filePath)
    {
        $config = [];
        foreach (\simplexml_load_file($filePath)->type as $type) {
            $arguments = [];
            foreach ($type->argument as $argument) {
                if (!isset($arguments['parameters'])) {
                    $arguments['parameters'] = [];
                }
                $arguments['parameters'][(string)$argument->name] = (string)$argument->value;
            }
            if (!isset($config["types"])) {
                $config["instance"] = [];
            }
            $config = \array_merge_recursive($config,
                                             [
                                                 "instance" => [
                                                     (string)$type->className => $arguments,
                                                 ],
                                             ]);
        }
        return $config;
    }

    /**
     * @param string $className
     * @param array  $params
     * @return object|null
     */
    public function get(string $className, array $params = [])
    {
        return $this->di->get(\trim($className, '\\'), $params);
    }

    /**
     * Retrieve a new instance of a class
     *
     * @param string $className
     * @param array  $params
     * @return object|null
     */
    public function create(string $className, array $params = [])
    {
        return $this->di->newInstance(\trim($className, '\\'), $params);
    }
}
