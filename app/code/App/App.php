<?php
/***********************************************************************************************************************
 * @package		Learning
 * @author		Zahirul Hasan<info@zbabu.com>
 * @copyright	Copyright (c) 2011 - 2019 @ Zahirul Hasan (http://zbabu.com) All rights reserved.
 * @license		http://zbabu.com/license-agreement
 **********************************************************************************************************************/
namespace TicTacToe\App;

class App
{

    /**
     * @var \Zend\Http\PhpEnvironment\Request
     */
    protected $request;

    /**
     * @var \TicTacToe\App\ComponentRegistrar
     */
    protected $componentRegistrar;

    /**
     * @var \TicTacToe\App\ObjectManager
     */
    protected $objectManager;

    public function __construct(
        \Zend\Http\PhpEnvironment\Request $request,
        \TicTacToe\App\ComponentRegistrar $componentRegistrar,
        ObjectManager $objectManager
    ) {
        $this->request = $request;
        $this->componentRegistrar = $componentRegistrar;
        $this->objectManager = $objectManager;
    }

    public function launch()
    {
        $path = \trim($this->request->getUri()->getPath(), "//");
        list($route, $path, $subPath) = \explode("/", $path . "///");
        $route = (!empty($route) ? $route : "tictactoe");
        $path = !empty($path) ? \ucfirst($path) : "Index";
        $subPath = !empty($subPath) ? \ucfirst($subPath) : "Index";
        $controller = $this->componentRegistrar->getNameSpaceByRoute($route);
        if (empty($controller)) {
            throw new \Exception("Route '" . $route . "'Doesn't Exist");
        }
        $controller .= "\\Controller\\" . $path . "\\" . $subPath;
        /**
         * @var ControllerInterface $controller
         */
        $controller = $this->objectManager->get($controller);
        $controller->dispatch();
    }
}
