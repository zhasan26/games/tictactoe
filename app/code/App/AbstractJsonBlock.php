<?php
/***********************************************************************************************************************
 * @package		Learning
 * @author		Zahirul Hasan<info@zbabu.com>
 * @copyright	Copyright (c) 2011 - 2019 @ Zahirul Hasan (http://zbabu.com) All rights reserved.
 * @license		http://zbabu.com/license-agreement
 **********************************************************************************************************************/

namespace TicTacToe\App;

/**
 * Class AbstractBlock
 * @package TicTacToe
 */
abstract class AbstractJsonBlock extends AbstractBlock
{
    /**
     * @var object|null
     */
    protected $data;

    /**
     * @var array
     */
    protected $childBlocks;

    /**
     * AbstractJsonBlock constructor.
     * @param \TicTacToe\App\Context\BlockContext $context
     * @param \TicTacToe\App\DataObjectFactory    $dataObjectFactory
     * @param array                              $childBlocks
     */
    public function __construct(
        \TicTacToe\App\Context\BlockContext $context,
        \TicTacToe\App\DataObjectFactory $dataObjectFactory,
        array $childBlocks = []
    ) {
        $this->data = $dataObjectFactory->create();
        parent::__construct($context, $childBlocks);
    }

    /**
     * @return string
     */
    public function getHtml(): string
    {
        $this->addHeader('Content-Type', 'application/json');
        return \json_encode($this->data->getData());
    }
}
