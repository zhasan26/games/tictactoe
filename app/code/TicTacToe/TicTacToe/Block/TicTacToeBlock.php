<?php
/***********************************************************************************************************************
 * @package		Learning
 * @author		Zahirul Hasan<info@zbabu.com>
 * @copyright	Copyright (c) 2011 - 2019 @ Zahirul Hasan (http://zbabu.com) All rights reserved.
 * @license		http://zbabu.com/license-agreement
 **********************************************************************************************************************/

namespace TicTacToe\TicTacToe\Block;

/**
 * Class TicTacToeBlock
 * @package TicTacToe
 */
class TicTacToeBlock extends \TicTacToe\App\AbstractBlock
{
    public function __construct(
        \TicTacToe\App\Context\BlockContext $context,
        array $childBlocks = []
    ) {
        parent::__construct($context, $childBlocks);
        $session = $context->getSession();
        if (empty($session->gameData)
            || empty($session->gameData['names']['p1'])
            || empty($session->gameData['names']['p2'])
        ){

            //$this->addHeader('Location: ', '/TicTacToe/game/start');
        };
        //die();
    }

    /**
     * TicTacToeBlock constructor.
     */
    public function _construct()
    {
        $this->setTemplate("TicTacToe_TicTacToe::TicTacToe.phtml");
    }

    /**
     * @return string
     */
    protected function renderBorad(): string
    {
        $boardHtml = '';
        for ($i = 0; $i< 3; $i++) {
            $boardHtml .= '<div class="row">';
            for ($j = 0; $j< 3; $j++) {
                $boardHtml .= '<div class="col-4 field" id="field-' . $i . $j . '"></div>';
            }
            $boardHtml .= '</div>';
        }
        return $boardHtml;
    }
}
