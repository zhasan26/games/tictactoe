<?php
/***********************************************************************************************************************
 * @package		Learning
 * @author		Zahirul Hasan<info@zbabu.com>
 * @copyright	Copyright (c) 2011 - 2019 @ Zahirul Hasan (http://zbabu.com) All rights reserved.
 * @license		http://zbabu.com/license-agreement
 **********************************************************************************************************************/

namespace TicTacToe\TicTacToe\Block;

/**
 * Class TicTacToeBlock
 * @package TicTacToe
 */
class GameBlock extends \TicTacToe\App\AbstractJsonBlock
{
    const MODE_VS = 0;
    const MODE_COMPUTER = 0;
    /**
     * @var \TicTacToe\App\SessionManager
     */
    protected $session;

    /**
     * GameBlock constructor.
     * @param \TicTacToe\App\Context\BlockContext $context
     * @param \TicTacToe\App\DataObjectFactory    $dataObjectFactory
     * @param array                              $childBlocks
     */
    public function __construct(
        \TicTacToe\App\Context\BlockContext $context,
        \TicTacToe\App\DataObjectFactory $dataObjectFactory,
        array $childBlocks = []
    ) {
        $this->session = $context->getSession();
        parent::__construct($context, $dataObjectFactory, $childBlocks);
    }

    public function _construct()
    {
        if (empty($this->session->boardData)
            || empty($this->session->currentPlayer)
            || empty($this->session->gameData)
        ) {
            $this->reset();
        }
    }

    public function getHtml(): string
    {
        $this->processGame();
        return parent::getHtml();
    }

    public function reset()
    {
        $this->session->boardData = [
            'p1' => ['iconClass' => 'learn-icon-x', 'pieces' => []],
            'p2' => ['iconClass' => 'learn-icon-o', 'pieces' => []],
        ];
        $this->session->gameData = [
            'names'=>['p1'=>'','p2'=>''],
            'scores'=>['p1'=>0,'p2'=>0],
            'mode'=> self::MODE_VS
        ];
        $this->session->currentPlayer = 'p1';
    }

    public function move(string $filedId)
    {
        $boardData = $this->session->boardData;
        $boardData[$this->session->currentPlayer]['pieces'][] = \ltrim($filedId, 'field-');
        $this->session->boardData = $boardData;
        $this->session->currentPlayer = ($this->session->currentPlayer == 'p1') ? 'p2' : 'p1';
    }

    /**
     * @return string
     */
    protected function processGame()
    {
        return $this->renderBorad();
    }
    /**
     * @return string
     */
    protected function renderBorad()
    {
        $board = [];
        for ($i = 0; $i < 3; $i++) {
            $board[$i] = [];
            for ($j = 0; $j < 3; $j++) {
                $id = $i . $j;
                $class = "";
                $allowMove = true;
                foreach ($this->session->boardData as $boardData) {
                    if (\in_array($id, $boardData['pieces'])) {
                        $class = $boardData['iconClass'];
                        $allowMove = false;
                        break;
                    }
                }
                $board[$i][$j] = [
                    "id"=>'field-' . $id,
                    'iconClass'=> $class,
                    'allowMove' => $allowMove
                ];
            }
        }
        $this->setBoard($board);
    }

    /**
     * @param array $board
     * @return $this
     */
    protected function setBoard(array $board)
    {
        $this->data->setBoard($board);
        return $this;
    }
}
