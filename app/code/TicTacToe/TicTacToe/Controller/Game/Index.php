<?php
/***********************************************************************************************************************
 * @package		Learning
 * @author		Zahirul Hasan<info@zbabu.com>
 * @copyright	Copyright (c) 2011 - 2019 @ Zahirul Hasan (http://zbabu.com) All rights reserved.
 * @license		http://zbabu.com/license-agreement
 **********************************************************************************************************************/

namespace TicTacToe\TicTacToe\Controller\Game;

/**
 * Class Index
 * @package TicTacToe\TicTacToe\Controller\Index
 */
class Index extends \TicTacToe\App\AbstractController
{
    /**
     * Index constructor.
     * @param \TicTacToe\App\Context\ControllerContext $context
     * @param \TicTacToe\TicTacToe\Block\TicTacToeBlock          $block
     */
    public function __construct(
        \TicTacToe\App\Context\ControllerContext $context,
        \TicTacToe\TicTacToe\Block\GameBlock $block
    ) {
        parent::__construct($context, $block);
    }
}
